import sys
from optparse import OptionParser

class GetOpts(object):
  def __init__(self, name='', version='1.0'):
    self.name = name
    self.version = version
    self.parser = OptionParser(version=self.version,
      usage = '\n'.join([self.name + ' [Options] \n',
                         'This script parses through all of the jobs, ',
                          'works out the codebase and the job name, grabs',
                          'this information and then queries Jenkins']))

  def username(self):
    self.parser.add_option('-u', '--username', action='store',
                            help='Please provide a valid Username')
  def token(self):
    self.parser.add_option('-t', '--token', action='store',
                            help='Please provide a valid Token')

  def password(self):
    self.parser.add_option('-p', '--password', action='store',
                            help='Please provide a valid password')
  def set_opts(self):
    options, args = self.parser.parse_args()
    return options, args, self.parser

class CheckOpts(object):
  def __init__(self, parser=None, options=None):
    self.options = options
    self.parser = parser

  def username(self):
    try:
      username = self.options.username
    except AttributeError:
      self.parser.print_help()
      print("\n\t*** Please provide a valid Jenkins Username ***\n")
      sys.exit(1)

    if not self.options.username:
      self.parser.print_help()
      print("\n\t*** Please provide a valid Jenkins Username ***\n")
      sys.exit(1)

  def password(self):
    try:
      password = self.options.password
    except AttributeError:
        try:
          password = "token"
          token = self.options.token
        except AttributeError:
          self.parser.print_help()
          print("\n\t*** Please provide either a valid auth Token or Password ***\n")
          sys.exit(1)

    if not self.options.password:
      self.parser.print_help()
      print("\n\t*** Please provide a valid auth Token or Password ***\n")
      sys.exit(1)

  def token(self):
    try:
      token = self.options.token
    except AttributeError:
        try:
          token = "password"
          password = self.options.password
        except AttributeError:
          self.parser.print_help()
          print("\n\t*** Please provide either a valid auth Token or Password ***\n")
          sys.exit(1)

    if not self.options.token:
      self.parser.print_help()
      print("\n\t*** Please provide a valid auth Token or Password ***\n")
      sys.exit(1)

