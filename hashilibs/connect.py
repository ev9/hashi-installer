import os
import sys
import logging
import requests
from requests.auth import HTTPBasicAuth
import json
import requests.packages.urllib3
requests.packages.urllib3.disable_warnings()

# generic connection class
class Connect(object):
    def __init__(self, config=None):
        self.session = None
        self.good_status = [ 200, 201, 202, 203, 204 ]

        try:
            self.username = config['username']
            password = config['token']
        except KeyError as err:
            logging.warning("Skipping Auth :: %s", err)
            self.username = ''
            password=''

        try:
            self.headers = config['html']['headers']
        except KeyError:
            self.headers = {'Content-type': 'application/json',
                            'accept': 'application/json'}

        try:
            self.verify = config["html"]["ssl_verify"]
        except KeyError:
            self.verify = False

        try:
            self.auth_basic = HTTPBasicAuth(self.username, password)
        except:
            self.auth_basic = ''

    def get_html(self, url=None):
        if not url:
            logging.error("Error :: Please Provide a url: %s", url)
            sys.exit(1)
        response = requests.get(url)
        status_code = response.status_code
        return response.text, response.content

    def get_basic(self, url=None):
        if not url:
            logging.error("Error :: Please Provide a url: %s", url)
            sys.exit(1)

        response = requests.get(url,
                                headers=self.headers,
                                auth=self.auth_basic,
                                verify=self.verify)
        status_code = response.status_code

        if status_code in self.good_status:
            json_data = response.json()
            return json_data
        else:
            logging.warning("URL: %s :: Received Status Code: %s",
                            url, status_code)
            return None

