import os
import yaml
import json


class IOHandler(object):
    def __init__(self, options=None, parser=None):
        self.options=options
        self.parser=parser

    def read_json(self, config_file=None):
        #cfg_path = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
        #json_file = os.path.join(cfg_path, config)
        with open(config_file) as file:
            json_data = json.load(file)
        return json_data

    def read_txt(self, config=None):
        with open(config, 'r') as output:
            txt_data = output.read()
        return txt_data

    def read_yaml(self, config_file=None):
        cfg_path = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
        yaml_file = os.path.join(cfg_path, config_file)
        with open(yaml_file, "r") as config:
            yaml_data = yaml.safe_load(config)
        return yaml_data

    def gen_yaml(self, data=None, output_file=None):
        output_dir, file_name = os.path.split(output_file)
        if not os.path.exists(output_dir):
            os.makedirs(output_dir)
        with open(output_file, 'w') as output:
            output.write(yaml.safe_dump(data, encoding='utf-8', allow_unicode=True, default_flow_style=False))

    def gen_json(self, data=None, output_file=None):
        output_dir, file_name = os.path.split(output_file)
        if not os.path.exists(output_dir):
            os.makedirs(output_dir)
        with open(output_file, 'w') as json_out:
            json.dump(data, json_out, indent=4)

    def create_dir(self, output_dir=None, file_name=None):
        path = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
        output_path = os.path.join(path, output_dir)
        if not os.path.exists(output_path):
            os.makedirs(output_path)
        if file_name:
            output_file = os.path.join(output_path, file_name)
            return output_file
        return output_path

