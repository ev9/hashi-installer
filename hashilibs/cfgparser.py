import sys

class Config(object):
    def __init__(self, options=None, parser=None):
        self.options = options
        self.parser = parser

    def process(self, data=None):
        data['options'] = self.options
        return data

